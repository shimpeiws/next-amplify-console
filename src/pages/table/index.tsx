import React from "react";
import Link from "next/link";

const Index: React.VFC = () => {
  return (
    <>
      <div>
        <Link href="/table/component">Component</Link>
      </div>
      <div>
        <Link href="/table/view-model">ViewModel</Link>
      </div>
    </>
  );
};

export default Index;
