import React from "react";
import { useTable, RowData } from "../../hooks/useTable";
import { TablePage } from "../../components/TablePage";

const ViewModel: React.VFC = () => {
  const [data, setData] = React.useState<RowData[]>([]);
  const { getViewModeldata } = useTable();

  React.useEffect(() => {
    const fetchData = async () => {
      console.info("getViewModeldata", getViewModeldata);
      const res = await getViewModeldata();
      setData(res.data);
    };

    fetchData();
  }, []);

  return null;

  // return (
  //   <>
  //     <p>ViewModel</p>
  //     <TablePage
  //       data={data}
  //       month={[
  //         "2021/4",
  //         "2021/5",
  //         "2021/6",
  //         "2021/7",
  //         "2021/8",
  //         "2021/9",
  //         "2021/10",
  //         "2021/11",
  //         "2021/12",
  //         "2022/1",
  //         "2022/2",
  //         "2022/3",
  //       ]}
  //     />
  //   </>
  // );
};

export default ViewModel;
