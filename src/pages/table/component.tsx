import React from "react";
import { useTable, Category, Item, MontylySales } from "../../hooks/useTable";
import { TableHeader } from "../../components/TableHeader";
import { TableRow } from "../../components/TableRow";

const Component: React.VFC = () => {
  const [categories, setCategories] = React.useState<Category[]>([]);
  const [items, setItems] = React.useState<Item[]>([]);
  const [monthlySales, setMonthlySales] = React.useState<MontylySales[]>([]);
  const { getTableData } = useTable();

  React.useEffect(() => {
    const fetchData = async () => {
      console.info("getTableData", getTableData);
      const res = await getTableData();
      setCategories(res.categories);
      setItems(res.items);
      setMonthlySales(res.sales);
    };

    fetchData();
  }, []);

  const itemsRowData = (category) => {
    const d = items
      .filter((item) => {
        return category.id === item.categoryId;
      })
      .map((item) => {
        const s = monthlySales
          .filter((sales) => {
            return sales.itemId === item.id;
          })
          .map((sales) => {
            return {
              year: sales.year,
              month: sales.month,
              amount: sales.amount,
            };
          });
        return {
          categoryName: undefined,
          itemName: item.name,
          sales: s,
        };
      });
    console.info("d", d);
    return d;
  };

  return null;

  // return (
  //   <>
  //     <p>Component</p>
  //     <table>
  //       <TableHeader
  //         headers={[
  //           "Category",
  //           "Item",
  //           "2021/4",
  //           "2021/5",
  //           "2021/6",
  //           "2021/7",
  //           "2021/8",
  //           "2021/9",
  //           "2021/10",
  //           "2021/11",
  //           "2021/12",
  //           "2022/1",
  //           "2022/2",
  //           "2022/3",
  //         ]}
  //       />
  //       {categories.map((category) => {
  //         return (
  //           <>
  //             <TableRow
  //               key={category.name}
  //               categoryName={category.name}
  //               itemName={undefined}
  //               sales={undefined}
  //             />
  //             {itemsRowData(category).map((d) => {
  //               return (
  //                 <TableRow
  //                   key={d.categoryName}
  //                   categoryName={undefined}
  //                   itemName={d.itemName}
  //                   // sales={d.sales}
  //                   sales={undefined}
  //                 />
  //               );
  //             })}
  //           </>
  //         );
  //       })}
  //     </table>
  //   </>
  // );
};

export default Component;
