import React from "react";
import { useTranslation } from "react-i18next";
import { Wrapper } from "../../styles/style";
import { Title } from "../../components/Title";
import { Button } from "../../components/Button";

const Hello: React.VFC = () => {
  const { i18n, t } = useTranslation();
  return (
    <Wrapper>
      <Title title={t("common.hello")} />
      <Button
        onClick={() => {
          console.info("i18n.language", i18n.language);
          i18n.changeLanguage(i18n.language === "en" ? "ja" : "en");
        }}
      >
        Submit
      </Button>
    </Wrapper>
  );
};

export default Hello;
