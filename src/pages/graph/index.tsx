import React from "react";
import { Bar } from "react-chartjs-2";

const Graph: React.VFC = () => {
  const options = {
    interaction: {
      mode: "x",
    },
    scales: {
      yAxes: [
        {
          stacked: true,
          ticks: {
            beginAtZero: true,
          },
        },
      ],
      xAxes: [
        {
          stacked: true,
        },
      ],
    },
    tooltips: {
      mode: "y",
    },
  };
  const arbitraryStackKey = "stack1";
  const arbitraryStackKey2 = "stack2";
  const graphData = {
    labels: ["1", "2", "3", "4", "5", "6"],
    datasets: [
      {
        stack: arbitraryStackKey,
        label: "# of A",
        data: [12, 19, 3, 5, 2, 3],
        backgroundColor: "rgb(255, 99, 132)",
        borderColor: "rgb(255, 255, 255, 1.0)",
        borderWidth: 1,
      },
      {
        stack: arbitraryStackKey,
        label: "# of B",
        data: [2, 3, 20, 5, 1, 4],
        backgroundColor: "rgb(255, 162, 235)",
        borderColor: "rgb(255, 255, 255, 1.0)",
        borderWidth: 1,
      },
      {
        stack: arbitraryStackKey2,
        label: "# of C",
        data: [3, 10, 13, 15, 22, 30],
        backgroundColor: "rgb(75, 192, 192)",
        borderColor: "rgb(255, 255, 255, 1.0)",
        borderWidth: 1,
      },
      {
        stack: arbitraryStackKey2,
        label: "# of D",
        data: [10, 2, 13, 1, 12, 3],
        backgroundColor: "rgb(75, 192, 0)",
        borderColor: "rgb(255, 255, 255, 1.0)",
        borderWidth: 1,
        hoverBackgroundColor: "rgb(180, 63, 0)",
      },
      {
        stack: arbitraryStackKey2,
        label: "# of E",
        data: [3, 10, 13, 15, 22, 30],
        backgroundColor: "rgb(75, 192, 0)",
        borderColor: "rgb(255, 255, 255, 1.0)",
        borderWidth: 1,
        hoverBackgroundColor: "rgb(180, 63, 0)",
      },
    ],
  };

  return (
    <div className="App">
      {/* グラフコンポーネントの呼び出し */}
      <Bar data={graphData} options={options} />
    </div>
  );
};

export default Graph;
