import React from "react";
import Link from "next/link";

const Index: React.VFC = () => {
  return (
    <>
      <p>Index</p>
      <div>
        <Link href="/hello">hello</Link>
      </div>
      <div>
        <Link href="/table">Table</Link>
      </div>
      <div>
        <Link href="/graph">Graph</Link>
      </div>
    </>
  );
};

export default Index;
