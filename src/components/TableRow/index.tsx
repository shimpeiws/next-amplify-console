import React from "react";
import { YearlySales } from "../../hooks/useTable";

type Props = {
  categoryName: string | undefined;
  itemName: string | undefined;
  sales: YearlySales | undefined;
};

export const TableRow: React.VFC<Props> = (props) => {
  const { categoryName, itemName, sales } = props;

  return (
    <>
      <tr key={`${categoryName}-${itemName}`}>
        <td>{categoryName ? categoryName : ""}</td>
        <td>{itemName ? itemName : ""}</td>
        {sales ? (
          sales.map((s, index) => {
            return (
              <>
                <td key={`${categoryName}-${itemName}-${index}`}>{s.amount}</td>
              </>
            );
          })
        ) : (
          <td key={`${categoryName}-${itemName}`}></td>
        )}
      </tr>
    </>
  );
};
