import React from "react";

type Props = {
  headers: string[];
};

export const TableHeader: React.VFC<Props> = (props) => {
  return (
    <>
      <tr>
        {props.headers.map((h) => {
          return <th key={h}>{h}</th>;
        })}
      </tr>
    </>
  );
};
