import styled from "styled-components";

type BaseButtonProps = {
  height?: string;
  width?: string;
};

export const BaseButton = styled.button<BaseButtonProps>`
  height: ${(props) => props.height || "36px"};
  width: ${(props) => props.width || "100%"};
  padding: 10px 16px;
  font-size: 14px;
  color: #4790bb;
  border: none;
  border-radius: 8px;
  line-height: 1;
  transition: 0.2s;
`;
