import React from "react";
import { BaseButton } from "./style";

type Props = {
  children: React.ReactNode;
  width?: string;
  height?: string;
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
};

export const Button: React.VFC<Props> = (props) => {
  return (
    <BaseButton
      width={props.width}
      height={props.height}
      onClick={props.onClick}
    >
      {props.children}
    </BaseButton>
  );
};
