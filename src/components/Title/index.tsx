import React from "react";
import { BaseTitle } from "./style";

type Props = {
  title: string;
};

export const Title: React.VFC<Props> = (props) => {
  return <BaseTitle>{props.title}</BaseTitle>;
};
