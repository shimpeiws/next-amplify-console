import styled from "styled-components";

export const BaseTitle = styled.h1`
  font-size: 32px;
  color: #4790bb;
`;
