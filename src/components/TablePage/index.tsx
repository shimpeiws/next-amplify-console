import React from "react";
import { RowData } from "../../hooks/useTable";
import { TableHeader } from "../../components/TableHeader";
import { TableRow } from "../TableRow";

type Props = {
  month: string[];
  data: RowData[];
};

export const TablePage: React.VFC<Props> = (props) => {
  return (
    <>
      <table>
        <TableHeader headers={["Category", "Item", ...props.month]} />
        {props.data.map((row) => {
          return (
            <>
              <TableRow
                key={`${row.categoryName}-${row.itemName}`}
                categoryName={row.categoryName}
                itemName={row.itemName}
                sales={row.sales}
              />
            </>
          );
        })}
      </table>
    </>
  );
};
