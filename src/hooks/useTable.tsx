export type Category = {
  id: number;
  name: string;
};

export type Item = {
  id: number;
  categoryId: number;
  name: string;
};

export type MontylySales = {
  id: number;
  itemId: number;
  year: number;
  month: number;
  amount: number;
};

type TableData = {
  categories: Category[];
  items: Item[];
  sales: MontylySales[];
};

type MonthlySales = {
  year: number;
  month: number;
  amount: number | undefined;
};

export type YearlySales = [
  MonthlySales,
  MonthlySales,
  MonthlySales,
  MonthlySales,
  MonthlySales,
  MonthlySales,
  MonthlySales,
  MonthlySales,
  MonthlySales,
  MonthlySales,
  MonthlySales,
  MonthlySales
];

export type RowData = {
  categoryName: string | undefined;
  itemName: string | undefined;
  sales: YearlySales | undefined;
};

type TableViewModel = {
  data: RowData[];
};

type UseGetCurrentUser = {
  getTableData: () => Promise<TableData>;
  getViewModeldata: () => Promise<TableViewModel>;
};

export const useTable = (): UseGetCurrentUser => {
  const getCategories = (): Promise<Category[]> => {
    // TODO: Connect to categories API
    return Promise.resolve([
      {
        id: 1,
        name: "Food",
      },
      {
        id: 2,
        name: "Book",
      },
      {
        id: 3,
        name: "Electric",
      },
    ]);
  };

  const getItems = (): Promise<Item[]> => {
    // TODO: Connect to items API
    return Promise.resolve([
      {
        id: 1,
        categoryId: 1,
        name: "Apple",
      },
      {
        id: 2,
        categoryId: 1,
        name: "Banana",
      },
      {
        id: 3,
        categoryId: 2,
        name: "Lean Startup",
      },
      {
        id: 4,
        categoryId: 2,
        name: "Team Geek",
      },
      {
        id: 5,
        categoryId: 2,
        name: "User Story Mapping",
      },
    ]);
  };

  const getMonthlySales = (): Promise<MontylySales[]> => {
    return Promise.resolve([
      {
        id: 1,
        itemId: 1,
        year: 2021,
        month: 4,
        amount: 1000,
      },
      {
        id: 2,
        itemId: 1,
        year: 2021,
        month: 5,
        amount: 1000,
      },
      {
        id: 3,
        itemId: 1,
        year: 2021,
        month: 6,
        amount: 1000,
      },
      {
        id: 4,
        itemId: 1,
        year: 2021,
        month: 7,
        amount: 1000,
      },
      {
        id: 5,
        itemId: 1,
        year: 2021,
        month: 8,
        amount: 1000,
      },
      {
        id: 6,
        itemId: 1,
        year: 2021,
        month: 9,
        amount: 1000,
      },
      {
        id: 7,
        itemId: 1,
        year: 2021,
        month: 10,
        amount: 1000,
      },
      {
        id: 8,
        itemId: 1,
        year: 2021,
        month: 11,
        amount: 1000,
      },
      {
        id: 9,
        itemId: 1,
        year: 2021,
        month: 12,
        amount: 1000,
      },
      {
        id: 10,
        itemId: 1,
        year: 2022,
        month: 1,
        amount: 1000,
      },
      {
        id: 11,
        itemId: 1,
        year: 2022,
        month: 2,
        amount: 1000,
      },
      {
        id: 12,
        itemId: 1,
        year: 2022,
        month: 3,
        amount: 1000,
      },
      {
        id: 13,
        itemId: 2,
        year: 2021,
        month: 4,
        amount: 0,
      },
      {
        id: 14,
        itemId: 2,
        year: 2021,
        month: 5,
        amount: 0,
      },
      {
        id: 15,
        itemId: 2,
        year: 2021,
        month: 6,
        amount: 0,
      },
      {
        id: 16,
        itemId: 2,
        year: 2021,
        month: 7,
        amount: 0,
      },
      {
        id: 17,
        itemId: 2,
        year: 2021,
        month: 8,
        amount: 0,
      },
      {
        id: 18,
        itemId: 2,
        year: 2021,
        month: 9,
        amount: 0,
      },
      {
        id: 19,
        itemId: 2,
        year: 2021,
        month: 10,
        amount: 0,
      },
      {
        id: 20,
        itemId: 2,
        year: 2021,
        month: 11,
        amount: 0,
      },
      {
        id: 21,
        itemId: 2,
        year: 2021,
        month: 12,
        amount: 0,
      },
      {
        id: 22,
        itemId: 2,
        year: 2022,
        month: 1,
        amount: 0,
      },
      {
        id: 23,
        itemId: 2,
        year: 2022,
        month: 2,
        amount: 0,
      },
      {
        id: 24,
        itemId: 2,
        year: 2022,
        month: 3,
        amount: 0,
      },
      {
        id: 25,
        itemId: 3,
        year: 2021,
        month: 3,
        amount: 0,
      },
      {
        id: 26,
        itemId: 3,
        year: 2021,
        month: 5,
        amount: 0,
      },
      {
        id: 27,
        itemId: 3,
        year: 2021,
        month: 6,
        amount: 0,
      },
      {
        id: 28,
        itemId: 3,
        year: 2021,
        month: 7,
        amount: 0,
      },
      {
        id: 29,
        itemId: 3,
        year: 2021,
        month: 8,
        amount: 0,
      },
      {
        id: 30,
        itemId: 3,
        year: 2021,
        month: 9,
        amount: 0,
      },
      {
        id: 31,
        itemId: 3,
        year: 2021,
        month: 10,
        amount: 0,
      },
      {
        id: 32,
        itemId: 3,
        year: 2021,
        month: 11,
        amount: 0,
      },
      {
        id: 33,
        itemId: 3,
        year: 2021,
        month: 12,
        amount: 0,
      },
      {
        id: 34,
        itemId: 3,
        year: 2022,
        month: 1,
        amount: 0,
      },
      {
        id: 35,
        itemId: 3,
        year: 2022,
        month: 2,
        amount: 0,
      },
      {
        id: 36,
        itemId: 3,
        year: 2022,
        month: 3,
        amount: 0,
      },
      {
        id: 37,
        itemId: 4,
        year: 2021,
        month: 4,
        amount: undefined,
      },
      {
        id: 38,
        itemId: 4,
        year: 2021,
        month: 5,
        amount: undefined,
      },
      {
        id: 39,
        itemId: 4,
        year: 2021,
        month: 6,
        amount: undefined,
      },
      {
        id: 40,
        itemId: 4,
        year: 2021,
        month: 7,
        amount: undefined,
      },
      {
        id: 41,
        itemId: 4,
        year: 2021,
        month: 8,
        amount: undefined,
      },
      {
        id: 42,
        itemId: 4,
        year: 2021,
        month: 9,
        amount: undefined,
      },
      {
        id: 43,
        itemId: 4,
        year: 2021,
        month: 10,
        amount: undefined,
      },
      {
        id: 44,
        itemId: 4,
        year: 2021,
        month: 11,
        amount: undefined,
      },
      {
        id: 45,
        itemId: 4,
        year: 2021,
        month: 12,
        amount: undefined,
      },
      {
        id: 46,
        itemId: 4,
        year: 2022,
        month: 1,
        amount: undefined,
      },
      {
        id: 47,
        itemId: 4,
        year: 2022,
        month: 2,
        amount: undefined,
      },
      {
        id: 48,
        itemId: 4,
        year: 2022,
        month: 3,
        amount: undefined,
      },
      {
        id: 49,
        itemId: 5,
        year: 2021,
        month: 4,
        amount: 10000000.11,
      },
      {
        id: 50,
        itemId: 5,
        year: 2021,
        month: 5,
        amount: 10000000.11,
      },
      {
        id: 51,
        itemId: 5,
        year: 2021,
        month: 6,
        amount: 10000000.11,
      },
      {
        id: 52,
        itemId: 5,
        year: 2021,
        month: 7,
        amount: 10000000.11,
      },
      {
        id: 53,
        itemId: 5,
        year: 2021,
        month: 8,
        amount: 10000000.11,
      },
      {
        id: 54,
        itemId: 5,
        year: 2021,
        month: 9,
        amount: 10000000.11,
      },
      {
        id: 55,
        itemId: 5,
        year: 2021,
        month: 10,
        amount: 10000000.11,
      },
      {
        id: 56,
        itemId: 5,
        year: 2021,
        month: 11,
        amount: 10000000.11,
      },
      {
        id: 57,
        itemId: 5,
        year: 2021,
        month: 12,
        amount: 10000000.11,
      },
      {
        id: 58,
        itemId: 5,
        year: 2022,
        month: 1,
        amount: 10000000.11,
      },
      {
        id: 59,
        itemId: 5,
        year: 2022,
        month: 2,
        amount: 10000000.11,
      },
      {
        id: 60,
        itemId: 5,
        year: 2022,
        month: 3,
        amount: 10000000.11,
      },
    ]);
  };

  const getTableData = async (): Promise<TableData> => {
    const res = await Promise.all([
      getCategories(),
      getItems(),
      getMonthlySales(),
    ]);
    return {
      categories: res[0],
      items: res[1],
      sales: res[2],
    };
  };

  const rowData = (
    categories: Category[],
    items: Item[],
    sales: MontylySales[]
  ): RowData[] => {
    const data = categories
      .map((category) => {
        const categoryRowData = items
          .filter((item) => {
            return category.id === item.categoryId;
          })
          .map((item) => {
            const s = sales
              .filter((monthlySales) => {
                return monthlySales.itemId === item.id;
              })
              .map((monthlySales) => {
                return {
                  year: monthlySales.year,
                  month: monthlySales.month,
                  amount: monthlySales.amount,
                };
              });
            return {
              categoryName: undefined,
              itemName: item.name,
              sales: s,
            };
          });
        categoryRowData.unshift({
          categoryName: category.name,
          itemName: undefined,
          sales: undefined,
        });
        return categoryRowData;
      })
      .flat();
    return data as RowData[];
  };

  const getViewModeldata = async (): Promise<TableViewModel> => {
    const res = await Promise.all([
      getCategories(),
      getItems(),
      getMonthlySales(),
    ]);
    return {
      data: rowData(res[0], res[1], res[2]),
    };
  };

  return {
    getTableData,
    getViewModeldata,
  };
};
