import styled from "styled-components";

export const Wrapper = styled.div`
  width: 100%;
  max-width: 540px;
  margin: 52px auto 80px;
  padding: 40px 48px 48px;
  box-shadow: 0 2px 6px 0 rgba(0, 0, 0, 0.2);
  border-radius: 24px;
  overflow: hidden;
`;
